lrSnippet = require("grunt-contrib-livereload/lib/utils").livereloadSnippet
mountFolder = (connect, dir) ->
  connect.static require("path").resolve(dir)

module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    sass:
      dev:
        options:
          style: "expanded"
          lineNumbers: true
          trace: true

        files:
          "static/assets/css/_main.css": "sass/main.scss"
          "static/assets/css/_capsule.css": "sass/capsule.scss"
    jade:
      compile:
        options:
          pretty: true

        files:
          "static/index.html": "jade/index.jade"
          "static/lmdvvyd.html": "jade/lmdvvyd.jade"
          "static/templates/svg-img.html": "jade/templates/svg-img.jade"

    coffee:
      compile:
        options:
          bare: true

        files:
          "static/assets/js/_main.js": "coffee/main.coffee"
          "static/assets/js/_news.js": "coffee/news.coffee"

    regarde:
      less:
        files: ["sass/*"]
        tasks: ["sass", "livereload"]

      jade:
        files: ["jade/*.jade", "jade/templates/*"]
        tasks: ["jade", "livereload"]

      coffee:
        files: ["coffee/*"]
        tasks: ["coffee", "livereload"]

    connect:
      
      #server: {
      #				options: {
      #					port: 8192,
      #					base: 'static'
      #				}	
      #			}
      livereload:
        options:
          port: 8192
          base: "static"
          middleware: (connect, options) ->
            [lrSnippet, mountFolder(connect, options.base)]

  grunt.loadNpmTasks "grunt-contrib-livereload"
  grunt.loadNpmTasks "grunt-contrib-connect"
  grunt.loadNpmTasks "grunt-regarde"
  grunt.loadNpmTasks "grunt-contrib-sass"
  grunt.loadNpmTasks "grunt-contrib-jade"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.registerTask "default", ["sass", "jade", "coffee", "livereload-start", "connect", "regarde"]