Newslister = ($http,$scope) ->
	$scope.dis = off
	$scope.success = off
	$scope.fail = ""
	$scope.addListener = ->
		$http.post("http://corditestudios.com/addsubscriber.php",
			email: $scope.email
		).success (data) ->
			$scope.fail = ""
			switch data
				when "Yes"
					$scope.dis = on
					$scope.success = on
				when "No"
					$scope.success = off
					$scope.fail = "Incorrect email"
				when "Already"
					$scope.dis = on
					$scope.success = on
				