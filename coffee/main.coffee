# Hello there

angular.module("cordite", [])
	.directive "svgImage", () ->
		directive =
			replace: false
			restrict: "A"
			link: (scope, element, attrs) ->
				scope.svgSupport = Modernizr.svg
				if(!scope.svgSupport)
					element.attr 'src', attrs.src.replace(/\.svg[z]{0,1}$/, '.png');
				null

		# return the directive object
		directive